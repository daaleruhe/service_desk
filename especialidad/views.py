from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Especialidad
from .forms import EspecialidadForm



# Create your views here.

class EspecialidadList(LoginRequiredMixin, ListView):
    model = Especialidad
    template_name = "especialidad/index.html"

class EspecialidadCreate(LoginRequiredMixin, CreateView):
    model = Especialidad
    form_class = EspecialidadForm
    success_url = reverse_lazy('especialidad:list')
    template_name = 'especialidad/form.html'

class EspecialidadUpdate(LoginRequiredMixin, UpdateView):
    model = Especialidad
    form_class = EspecialidadForm
    success_url = reverse_lazy('especialidad:list')
    template_name = 'especialidad/update.html'

class EspecialidadDelete(LoginRequiredMixin, DeleteView):
    model = Especialidad
    form_class = EspecialidadForm
    success_url = reverse_lazy('especialidad:list')
    template_name = 'especialidad/delete.html'