from django.urls import path
from django.views.generic import CreateView

from .views import EspecialidadList, EspecialidadCreate, EspecialidadUpdate, EspecialidadDelete

app_name = 'especialidad'
urlpatterns = [
    path('list/', EspecialidadList.as_view(), name='list'),
    path('create/', EspecialidadCreate.as_view(), name='create'),
    path('update/<int:pk>/', EspecialidadUpdate.as_view(), name='update'),
    path('delete/<int:pk>/', EspecialidadDelete.as_view(), name='delete'),
]