from django.contrib import admin
from .models import Especialidad

# Register your models here.

@admin.register(Especialidad)
class EspecialidadAdmin(admin.ModelAdmin):
    pass
