from django.db import models
from datetime import date
from django.contrib.auth.models import UserManager

hoy = date.today()


class UserQuerySet(models.QuerySet):

    def tecnicos(self):
        return self.filter(tipo_usuario=2)

    def empleados(self):
        return self.filter(tipo_usuario=0)


class UserManager(UserManager):
    def get_queryset(self):
        return UserQuerySet(self.model, using=self._db)

    def tecnicos(self):
        return self.get_queryset().tecnicos()
    
    def empleados(self):
        return self.get_queryset().empleados()
