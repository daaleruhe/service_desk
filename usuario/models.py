from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import BaseUserManager

from especialidad.models import Especialidad
from area.models    import Area
from .managers import UserManager

class User(AbstractUser):
    tipo = [
        ( 0 , 'Empleado'),
        ( 1 , 'Call-Center'),
        ( 2 , 'Tecnico')
    ]

    tipo_usuario = models.SmallIntegerField(choices=tipo, null=True, blank=True)

    especialidad = models.ForeignKey(Especialidad,on_delete = models.CASCADE, null = True)
    area = models.ForeignKey(Area, on_delete = models.CASCADE, blank=True, null=True)

    tipos = UserManager()
    objects = BaseUserManager()

    def __str__(self):
        return self.first_name