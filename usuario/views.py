from django.shortcuts import render
from django.views.generic import ListView ,CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth import views as auth_views
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import User
from .forms import UserForm

# Create your views here.
class UserList(LoginRequiredMixin, ListView):
    model = User
    template_name = "usuario/index.html"

class TechnicalList(LoginRequiredMixin, ListView):
    model = User
    template_name = "usuario/index.html"
    queryset = User.tipos.tecnicos()

class EmployeeList(LoginRequiredMixin, ListView):
    model = User
    template_name = "usuario/index.html"
    queryset = User.tipos.empleados()

class UserCreate(LoginRequiredMixin, CreateView):
    model = User
    form_class = UserForm
    success_url = reverse_lazy('usuario:list')
    template_name = 'usuario/form.html'

class UserUpdate(LoginRequiredMixin, UpdateView):
    model = User
    form_class = UserForm
    success_url = reverse_lazy('usuario:list')
    template_name = 'usuario/update.html'

class UserDelete(LoginRequiredMixin, DeleteView):
    model = User
    form_class = UserForm
    success_url = reverse_lazy('usuario:list')
    template_name = 'usuario/delete.html'

class LoginView(auth_views.LoginView):
    redirect_authenticated_user = True

class LogoutView(auth_views.LogoutView):
    pass

