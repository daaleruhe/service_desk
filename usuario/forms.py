import re
from django.forms import ModelForm
from.models import User

class UserForm(ModelForm):

    fields_required = [
        'first_name', 'last_name',
        'email'

    ]
    
    class Meta:
        model = User
        fields = [
            'username',
            'password',
            'first_name',
            'last_name',
            'email',
            'especialidad',
            'tipo_usuario'
        ]

        labels = {
            'especialidad': 'Especialidad'
        }

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)

        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True

        for visible in self.visible_fields():
            attrs = {'class': 'form-control', 'placeholder': visible.label}

            visible.field.widget.attrs.update(attrs)

            if self.errors.get(visible.name, None):
                visible.field.widget.attrs.update({'class': 'form-control is-invalid'})
