from django.urls import path
from django.views.generic import CreateView
from .views import  UserList, TechnicalList, EmployeeList, UserCreate, UserUpdate, UserDelete, LoginView, LogoutView

app_name = 'usuario'
urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
     path('logout/', LogoutView.as_view(), name='logout'),
    path('list/', UserList.as_view(), name='list'),
    path('technical/', TechnicalList.as_view(), name='technical'),
    path('employee/', EmployeeList.as_view(), name='employee'),
    path('create/', UserCreate.as_view(), name='create'),
    path('update/<int:pk>/', UserUpdate.as_view(), name='update'),
    path('delete/<int:pk>/', UserDelete.as_view(), name='delete'),
]
