from django.shortcuts import render
from django.views.generic import ListView ,CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Equipo
from .forms import EquipoForm

# Create your views here.
class EquipoList(LoginRequiredMixin, ListView):
    model = Equipo
    template_name = "equipo/index.html"

class EquipoCreate(LoginRequiredMixin, CreateView):
    model = Equipo
    form_class = EquipoForm
    success_url = reverse_lazy('equipo:list')
    template_name = 'equipo/create.html'


class EquipoUpdate(LoginRequiredMixin, UpdateView):
    model = Equipo
    form_class = EquipoForm
    success_url = reverse_lazy('equipo:list')
    template_name = 'equipo/update.html'

class EquipoDelete(LoginRequiredMixin, DeleteView):
    model = Equipo
    form_class = EquipoForm
    success_url = reverse_lazy('equipo:list')
    template_name = 'equipo/delete.html'

