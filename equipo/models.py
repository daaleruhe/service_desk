from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model
# Create your models here.

class Equipo (models.Model):
    garantia = [
        ('6 meses', '6 meses'),
        ('1 año', '1 año'),
        ('2 años', '2 años'),
        ('No tiene garantía', 'No tiene garantía')
    ]


    nombre = models.CharField(max_length=15)
    serial = models.IntegerField(unique=True)
    procesador = models.CharField(max_length=15)
    ram = models.CharField(max_length=60)
    placa_base = models.CharField(max_length=60)
    sistema_operativo = models.CharField(max_length=60)
    garantia = models.CharField(max_length = 30, choices=garantia)

    empleado = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.nombre
