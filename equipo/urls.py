from django.urls import path
from django.views.generic import CreateView
from .views import EquipoList, EquipoCreate, EquipoUpdate,EquipoDelete

app_name = 'equipo'
urlpatterns = [
    path('list/', EquipoList.as_view(), name='list'),
    path('create/', EquipoCreate.as_view(), name='create'),
    path('update/<int:pk>/', EquipoUpdate.as_view(), name='update'),
    path('delete/<int:pk>/', EquipoDelete.as_view(), name='delete'),
]
