from django.shortcuts import render
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from .models import Area
from .forms import AreaForm


class AreaList(LoginRequiredMixin, ListView):
    model = Area
    template_name = "area/index.html"

class AreaCreate(LoginRequiredMixin, CreateView):
    model = Area
    form_class = AreaForm
    success_url = reverse_lazy('area:list')
    template_name = 'area/form.html'

class AreaUpdate(LoginRequiredMixin, UpdateView):
    model = Area
    form_class = AreaForm
    success_url = reverse_lazy('area:list')
    template_name = 'area/update.html'

class AreaDelete(LoginRequiredMixin, DeleteView):
    model = Area
    form_class = AreaForm
    success_url = reverse_lazy('area:list')
    template_name = 'area/delete.html'