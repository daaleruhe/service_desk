import re
from django.forms import ModelForm, forms

from .models import Area

class AreaForm(ModelForm):

    class Meta:
        model = Area
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(AreaForm, self).__init__(*args, **kwargs)

        for visible in self.visible_fields():
            attrs = {'class': 'form-control', 'placeholder': visible.label}

            visible.field.widget.attrs.update(attrs)

            if self.errors.get(visible.name, None):
                visible.field.widget.attrs.update({'class': 'form-control is-invalid'})