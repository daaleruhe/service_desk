from django.urls import path
from django.views.generic import CreateView
from .views import AreaList, AreaCreate, AreaUpdate, AreaDelete

app_name = 'area'
urlpatterns = [
    path('list/', AreaList.as_view(), name='list'),
    path('create/',AreaCreate.as_view(), name='create'),
    path('update/<int:pk>/', AreaUpdate.as_view(), name='update'),
    path('delete/<int:pk>/', AreaDelete.as_view(), name='delete'),

]