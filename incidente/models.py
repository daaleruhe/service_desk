from django.db import models

from django.conf import settings

from equipo.models import Equipo
from especialidad.models import Especialidad
from django.db.models import Manager

from .managers import IncidenteManager


class Incidente(models.Model):
    RANGO = [
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
    ]

    nombre = models.CharField(max_length=50)
    descripcion_del_empleado = models.TextField(null=True)
    descripcion_del_tecnico = models.TextField(null=True)
    estado = models.BooleanField()
    prioridad = models.IntegerField(choices=RANGO, default=1)
    fecha_inicio = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True, auto_now_add=False)

    solucion = models.TextField(null=True, blank=True)

    empleado = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='empleados'
    )

    tecnico = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='tecnicos',
        blank=True,
        null=True
    )

    equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE)
    tipo_problema = models.ForeignKey(Especialidad, on_delete=models.CASCADE, blank=True, null=True)

    estados = IncidenteManager()
    objects = Manager()

    class Meta:
        ordering = ('fecha_inicio',)

    def __str__(self):
        return self.nombre

    def mostrar_estado(self):
        if self.estado:
            mostrar_estado = "No Solucionado"
        else:
            mostrar_estado = "solucionado"
        return mostrar_estado

    def finalizar(self):
        if self.estado:
            self.estado = False
            self.save(update_fields=['estado'])


#hola mundo