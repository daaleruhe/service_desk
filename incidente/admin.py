from django.contrib import admin
from .models import Incidente

# Register your models here.

@admin.register(Incidente)
class IncidenteAdmin(admin.ModelAdmin):
    readonly_fields = ('fecha_inicio', 'ultima_modificacion',)
