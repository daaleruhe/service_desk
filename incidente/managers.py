from django.db import models

class IncidenteQuerySet(models.QuerySet):
    def valids(self):
        return self.filter(estado=True)

    def finisheds(self):
        return self.filter(estado=False)
    
class IncidenteManager(models.Manager):
    def get_queryset(self):
        return IncidenteQuerySet(self.model, using=self._db)

    def finisheds(self):
        return self.get_queryset().finisheds()

    def valids(self):
        return self.get_queryset().valids()

