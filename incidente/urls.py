from django.urls import path
from django.views.generic import CreateView
from .views import (IncidenteList ,IncidenteCreate,IncidenteDelete,IncidenteUpdate,
                    Estadisticas, IncidenteTechnicalList, IncidenteFinish, IncidenteFinishedList,
                    IncidenteFinishedTechnicalList, EstadisticasFechas, EstadisticasEmpleado)

app_name = 'incidente'
urlpatterns = [
    path('list/', IncidenteList.as_view(), name='list'),
    path('my-list/', IncidenteTechnicalList.as_view(), name='my-list'),
    path('finished/', IncidenteFinishedList.as_view(), name='list-finished'),
    path('myfinished/', IncidenteFinishedTechnicalList.as_view(), name='my-list-finished'),
    path('stats/', Estadisticas.as_view(), name='stats'),
    path('stats/fecha/', EstadisticasFechas.as_view(), name='stats-fecha'),
    path('stats/empleados/', EstadisticasEmpleado.as_view(), name='stats-empleado'),
    path('create/', IncidenteCreate.as_view(), name='create'),
    path('update/<int:pk>/', IncidenteUpdate.as_view(), name='update'),
    path('finish/<int:pk>/', IncidenteFinish.as_view(), name='finish'),
    path('delete/<int:pk>/', IncidenteDelete.as_view(), name='delete'),
]
