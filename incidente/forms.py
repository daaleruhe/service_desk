import re
from django.forms import ModelForm, HiddenInput
from .models import Incidente
from django.contrib.auth import get_user_model

Usuario = get_user_model()

class IncidenteForm(ModelForm):

    fields_required = [
        'tipo_problema',
        'tecnico',
    ]

    class Meta:
        model = Incidente

        fields = '__all__'

        exclude = ('solucion',)


    def __init__(self, *args, **kwargs):
        super(IncidenteForm, self).__init__(*args, **kwargs)

        self.fields['tipo_problema'].required = True
        self.fields['tecnico'].required = True

        self.fields['tecnico'].queryset = Usuario.tipos.tecnicos()
        self.fields['empleado'].queryset = Usuario.tipos.empleados()

        for visible in self.visible_fields():
            attrs = {'class': 'form-control', 'placeholder': visible.label}

            visible.field.widget.attrs.update(attrs)

            if self.errors.get(visible.name, None):
                visible.field.widget.attrs.update({'class': 'form-control is-invalid'})

class IncidenteFinishForm(ModelForm):
    class Meta:
        model = Incidente
        fields = ('estado',)