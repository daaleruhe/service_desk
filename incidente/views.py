import json
import datetime
import random
from itertools import groupby
from collections import OrderedDict
from django.shortcuts import render
from django.urls import reverse_lazy

from django.views.generic import ListView ,CreateView, UpdateView, DeleteView, TemplateView
from django.urls import reverse_lazy

from django.db.models import Count, F
from django.db.models.functions import TruncMonth
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Incidente
from .forms import IncidenteForm, IncidenteFinishForm
from django.contrib.auth import get_user_model


Usuario = get_user_model()

class IncidenteList(LoginRequiredMixin, ListView):
    model = Incidente
    template_name = "incidente/index.html"
    queryset = Incidente.estados.valids()

class IncidenteTechnicalList( IncidenteList):
    def get_queryset(self):
        qs = super().get_queryset().filter(tecnico=self.request.user.id)
        return qs

class IncidenteCreate(LoginRequiredMixin, CreateView):
    model = Incidente
    form_class = IncidenteForm
    success_url = reverse_lazy('incidente:list')
    template_name = 'incidente/form.html'


class IncidenteUpdate(LoginRequiredMixin, UpdateView):
    model = Incidente
    form_class = IncidenteForm
    success_url = reverse_lazy('incidente:list')
    template_name = 'incidente/update.html'


class IncidenteDelete(LoginRequiredMixin, DeleteView):
    model = Incidente
    form_class = IncidenteForm
    success_url = reverse_lazy('incidente:list')
    template_name = 'incidente/delete.html'


class Estadisticas(LoginRequiredMixin, TemplateView):
    template_name = 'incidente/estadisticas.html'


class EstadisticasFechas(TemplateView):
    template_name = 'incidente/estadisticas_stast.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        months = list(datetime.date(2019, i, 1).strftime('%B') for i in range(1, 13))

        incidents_per_mouth = list(Incidente.objects.annotate(
            month=TruncMonth('fecha_inicio')).values('month').annotate(
            incidents=Count('id')).values('month', 'incidents'))

        datagroup = OrderedDict((k, {k1: sum(list(i['incidents'] for i in v)) for k1, v in groupby(values, key=lambda x: x['month'].strftime("%B"))}) for k, values in groupby(incidents_per_mouth, key=lambda x: x['month'].year))

        datasets = []
        for year, datamonths in datagroup.items():
            color = self.colors()
            datasets.append({
                'label': '{}'.format(year),
                'fill': False,
                'lineTension': 0,
                'backgroundColor': 'rgb({0}, {1}, {2})'.format(*color),
                'borderColor': 'rgb({0}, {1}, {2})'.format(*color),
                'data': tuple(datamonths.get(value, 0) for value in months)
            })

        data = {
            'labels': months,
            'datasets': datasets
        }

        context.update(datasets_incidents=json.dumps(data))
        return context

    def colors(self):
        r = int(random.random() * 256)
        g = int(random.random() * 256)
        b = int(random.random() * 256)

        return r, g, b

class IncidenteFinishedList(LoginRequiredMixin, ListView):
    model = Incidente
    template_name = "incidente/index.html"
    queryset = Incidente.estados.finisheds()

class IncidenteFinishedTechnicalList(LoginRequiredMixin, ListView):
    model = Incidente
    template_name = "incidente/index.html"
    queryset = Incidente.estados.finisheds()
    
    def get_queryset(self):
        qs = super().get_queryset().filter(tecnico=self.request.user.id)
        return qs


class IncidenteFinish(LoginRequiredMixin, UpdateView):
    model = Incidente
    form_class = IncidenteFinishForm
    success_url = reverse_lazy('incidente:list-finished')
    template_name = 'incidente/update.html'

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.finalizar()
        form = self.get_form()
        return self.form_valid(form)

class EstadisticasEmpleado(TemplateView):
    template_name = 'incidente/estadisticas_doughnut.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        incidents_per_empleado = list(
            Incidente.objects.order_by('empleado__username').annotate(user=F('empleado__username')).values(
                'user').annotate(
                incidents=Count('id')).values('user', 'incidents'))

        datagroup = OrderedDict((item['user'], item['incidents']) for item in incidents_per_empleado)

        empleados = datagroup.keys()

        datasets = [{
            'data': list(datagroup.values())
        }]

        data = {
            'labels': list(empleados),
            'datasets': datasets
        }

        context.update(datasets_incidents=json.dumps(data))
        return context

    def colors(self):
        r = int(random.random() * 256)
        g = int(random.random() * 256)
        b = int(random.random() * 256)

        return r, g, b
